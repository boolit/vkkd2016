# -*- encoding: utf-8 -*-
##############################################################################
#    
#    Odoo, Open Source Management Solution
#
#    Author: Dainius Kaniava. Copyright: JSC Boolit
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################
from openerp import fields, models, api, _
from openerp.exceptions import Warning, ValidationError


class HrEmployeeCertificateType(models.Model):
    _name = "hr.employee.certificate.type"

    name = fields.Char("Type Name", required=True)


class HrEmployeeCertificateName(models.Model):
    _name = "hr.employee.certificate.name"

    name = fields.Char(required=True)


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    certificate_ids = fields.One2many("hr.employee.certificate",
        "employee_id", string="Certificates")


class HrEmployeeCertificate(models.Model):
    _name = "hr.employee.certificate"

    name_id = fields.Many2one("hr.employee.certificate.name", "Name",
        required=True)
    type_id = fields.Many2one("hr.employee.certificate.type", "Type", required=True)
    unique_nb = fields.Char('Unique Number')
    description = fields.Text("Description")
    valid_from = fields.Date("Valid From", required=True)
    valid_to = fields.Date("Valid To")
    extended_to = fields.Date("Extended To")
    employee_id = fields.Many2one("hr.employee", "Employee", required=True)

    @api.one
    @api.constrains('valid_from', 'valid_to')
    def _check_valid_from_to(self):
        if self.valid_from and self.valid_to:
            if self.valid_from > self.valid_to:
                raise Warning("Incorrect valid to date.")
