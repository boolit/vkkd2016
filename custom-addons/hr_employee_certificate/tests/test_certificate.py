# -*- coding: utf-8 -*-

from openerp.tests.common import TransactionCase
from openerp.exceptions import Warning


class TestCertificate(TransactionCase):

    def setUp(self):
        super(TestCertificate, self).setUp()
        self.certificate_model = self.env['hr.employee.certificate']
        self.certificate_name = self.env[
            'hr.employee.certificate.name'].create({'name': 'Test 1'})
        self.employee_id = self.ref('hr.employee_al')

    def test_constrains(self):
        data = {
            'name_id': self.certificate_name.id,
            'unique_nb': '110011',
            'type_id': 1,
            'valid_from': '2015-01-01',
            'valid_to': '2015-02-01',
            'is_valid': False,
            'employee_id': self.employee_id
        }
        self.certificate_model.create(data)
        with self.assertRaises(Warning):
            self.certificate_model.create(data)
