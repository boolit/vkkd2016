# Python verslo procesuose - odoo

## Dienotvarkė
* Įmonės pristatymas
* Verslo procesų apybraiža
* Praktinė dalis
    - Python ir odoo: inheritance ir ORM
    - Užduoties paaiškinimas
    - Darbo aplinkos aprodymas
    - Užduoties vykdymas
    - Antros užduoties paaiškinimas
* Klausimai ir atsakymai

## Programinė įranga
Virtualiztion software: [virtualbox](https://www.virtualbox.org/wiki/Downloads) 

Container software: [vagrant](https://www.vagrantup.com/downloads.html) 

git: [git](https://git-scm.com/downloads)

Jūsų mėgstamas kodo redaktorius ([SublimeText](http://www.sublimetext.com/), [vim](http://www.vim.org/), [PyCharm](https://www.jetbrains.com/pycharm/) ar kitas)

Pradiniai failai: https://bitbucket.org/boolit/vkkd2016

### Sistemos paruošimas darbui
Nesvarbu, kokia sistema naudojatės (Linux ar Windows), reikės turėti virtualizacijos platformą ir konteinerių valdklį. Virtualizavimui rekomenduoju VirtualBox - atviro kodo, nemokamą programą, veikiančią ant daugumos operacinių sistemų. Konteineriams valdyti reiks įsidiegti Vagrant.
Git nėra būtinas, bet palengvintų odoo konteinerio parsisiuntimą.

*Tolimesnės nuorodos priima, kad jūs jau turite virtualizacijos platformą bei konteinerių valdiklį įdiegtus ir veikiančius jūsų kompiuteryje.*

### Pradiniai failai
Parsisiunčiame odoo konteinerį. 
Turintieji git suveda `git clone https://bitbucket.org/boolit/vkkd2016.git` į komandinę eilutę ir jiems parsiunčiamas katalogas `vkkd2016`, turintis visus reikiamus failus. 
Neturintieji git nueina [šiuo adresu](https://bitbucket.org/boolit/vkkd2016) ir spaudžia Downloads -> Download repository. Išpakuokite, kur jums patogu.

### `vagrant up`
Įeiname į atsiųstąjį katalogą.
Komandinėje eilutėje suvedame `vagrant up` ir sulaukiame, kol bus parsiųsti konteineriai ir aptarnaujantys failai. Tai užruks, tad turėkite kantrybės.

vagrant\`as turėtų automatiškai atpažinti įdiegtą virtualizacijos platformą, bet jei taip neatsitiko, jam tai galima nurodyti vedant `vagrant up --provider virtualbox`.

### Jungiamės prie sistemos
vagrant valdomas `vagrant up`, `vagrant halt` bei `vagrant reload` (ir kitomis) komandomis.

Įjungus virtualiąją mašiną su `vagrant up`, kartu įsijungia ir odoo serveris, tad nieko papildomai daryti nebereikia.

odoo aplinka pasiekiama `http://localhost:8080` adresu. 

Pradžioje turėsite susikurti pirmąją duomenų bazę. Pavadinti ją galite kaip norite, taip pat - susikurti slaptažodį. Tačiau *Master Password* parašykite `odoo`, nes taip nurodyta konfigūravimo faile. 

Uždėkite varnelę ties *Load demonstration data*, ir duomenų bazės kūrimo metu bus sudiegti ir demonstraciniai duomenys. 

## Workshop!
Sveikiname turint veikiančią odoo sistemą! Judame prie dirbtuvių ir užduoties. 

Kad nereikėtų daug perrašinėti nuo ekrano, pateikiame dalinį modulį. `hr_employee_certificate` padėtas `custom-addons/` kataloge. Su šiais failais ir dirbsime dirbtuvių metu.

## Namų darbai

Naujo modulio kūrimui pasinaudokite šablonu. `boolit_template_module` padėtas `custom-addons/` kataloge.

[Nuoroda į namų darbų aprašymą.](https://bitbucket.org/snippets/boolitlimited/5r4jG)

## Oficiali odoo dokumentacija
Prieš eidami į dirbtuves, galite susipažinti su oficialia odoo dokumentacija:

* [Theme Tutorial](https://www.odoo.com/documentation/8.0/howtos/themes.html)
* [Building a Website](https://www.odoo.com/documentation/8.0/howtos/website.html)
* [Building a Module](https://www.odoo.com/documentation/8.0/howtos/backend.html) - ši dokumentacija dirbtuvių metu pravers labiausiai
* [Building Inerface Extensions](https://www.odoo.com/documentation/8.0/howtos/web.html)