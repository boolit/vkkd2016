# Praktinė užduotis (įvadas)

**slide: Įvadas į praktinę užduotį**

O dabar keliaukime prie Odoo iš programinės pusės.

Odoo - atviro kodo ERP ir CRM sistema, turinti atskirus kliento ir serverio komponentus ir anksčiau vadinosi OpenERP.

Akronimas ERP (Enterprise Resource Planning) reiškia 'verslo valdymo sistema'. Tai - programinė įranga, skirta kompiuterizuoti įmonės valdymą, galinti apimti ir integruotis į visus įmonės verslo procesus. Ją naudojant: 

- palengvėja verslo apskaitos vedimas, 
- efektyviau išnaudojami resursai,
- valdomi kontaktai, 
- užtikrinamas efektyvus tiekimo grandinės veikimas, 
- supaprastėja analitinės įmonės veiklos ataskaitų sudarymas.

Panašiai kaip TVS - Turinio Valdymo Sistemos (kaip, pavyzdžiui, WordPress, Drupal, Joomla), skirtos turinio valdymui, taip Verslo Valdymo Sistemos skirtos sužiūrėti visa, kas susiję su įmonės veikla.

**slide: Odoo + python**

Ir, kas svarbiausia mums šiose dirbtuvėse, ji parašyta Python'u. 

**slide: python kodo fragmentai** 

Python yra dinamiškai rašoma kalba, jos sintaksė švari, o kodas lengvai skaitomas dėl sprendimo naudoti reikšmingąją tuščią vietą vietoje kodo skyrybos ženklų, t.y. - kabliataškio. Kaip matote pavyzdyje - nė vieno. Visa tabuliacija turi reikšmę, ir jei sugalvotumėte patraukti kurią nors eilutę šonan - kodas nebeveiktų.

**slide: c++ ir python kodo fragmentai** 

Palyginti su c++, kuri yra statiškai rašoma, Python'u kodą rašyti yra paprasčiau, nes nereikia kiekvieną kartą nurodyti kintamųjų tipų (pažymėta oranžine spalva). Tai, deja, reiškia didesnį klaidų skaičių kodo veikimo metu, kai tuo tarpu statiškai rašomos kalbos kintamųjų tipų neatitikimus pastebi kompiliavimo metu.

**slide: Odoo griaučiai (framework)**

Keliaujame prie pačio Odoo.

Odoo yra pastatyta ant savos griaučių (framework) sistemos, pagrįstos objektiniu programavimu bei turinčios ORM - Objektų Ryšių Valdymo - savybes.

**slide: objektų paveldėjimo pvz arba diagrama**

Objektinis programavimas grįstas objektais, kurie turi savo laukus ir metodus, taip pat tėvus ir vaikus. Vaikai paveldėja tėvų savybes, bet gali turėti ir savas. Tai padeda sumažinti rašomo kodo kiekį, susisteminti funkcijas. Ir Odoo framework'as, praplėtęs bazinį Python'o funkcionalumą, tuo labai naudojasi.

**slide: Standartinis Python paveldėjimas**

Standartiškai Python'e giminystės ryšys nurodomas klasės kūrimo metu, nurodant tėvinę klasę kaip argumentą: pavyzdžiui čia `TėvinėKlasė` turi tėvą `object`, kuris yra bazinė klasė, iš kurios turi kilti Python klasės. `DukterinėsKlasės` tėvas - `Tėvinė klasė`.

```
class TėvinėKlasė(object):
    def TėvinisMetodas(self):
        print "Tėvinis!"

class DukterinėKlasė(TėvinėKlasė):
    def DukterinisMetodas(self):
        print "Dukterinis!"
```

Standartinių klasių metodai keičiami paveldint klasę, sukuriant metodą tokiu pat vardu, kaip ir tėvinėje klasėje. Tuomet iš Dukterinės klasės kviečiamas jau pakeistasis metodas. Tačiau tai reiškia, kad reikia naudoti naująją klasę, jei nori turėti naująjį funkcionalumą. 

**slide: Standartinis Python paveldėjimas pt2**

Iškvietus šiuos metodus, gauname, kad tėvinė klasė turi tėvinį metodą, dukterinė klasė turi abu metodu. Jei `TėvinėKlasė` norės naudotis dukteriniu metodu, jai tai nepavyks.

```
>>> TėvinėKlasė.TėvinisMetodas()
Tėvinis!
>>> DukterinėKlasė.DukterinisMetodas()
Dukterinis!
>>> DukterinėKlasė.TėvinimasMetodas()
Tėvinis!
>>> TėvinėKlasė.DukterinisMetodas()
Traceback (most recent call last):
AttributeError: 'TėvinėKlasė' object has no attribute 'DukterinisMetodas'
```

**slide: odoo paveldėjimas**

Metodų koregavimą Odoo išsprendė kitaip.

Odoo turi du pagrindinius standartinių klasių tipus, iš kurių kildinamos kitos klasės: `Model`, ir `TransientModel`. Paveldėtos klasės turi kintamąjį `_name`, kuris nurodo šios klasės pavadinimą vidinėje sistemoje. Visi šie pavadinimai yra surenkami ir katalogizuojami serverio įjungimo metu.

**slide odoo paveldėjimas pt2**

Toks katalogizavimas leidžia pasinaudoti surinktąja kartoteka ir, kuriant naują klasę, joje parašyti `_inherit` bei nurodyti klasės, kurią norima modifikuoti, pavadinimą. Tai reiškia, kad jau esantį funkcionalumą galima pakeisti kitoje vietoje ir tai atsispindės visoje sistemoje.

Kaip matote, prieš panaudojant `_inherit`, funkcija veikia vienaip, o po to - kitaip. Ir visa tai - nekeičiant pirmosios klasės kodo.

```
class PirmaKlasė(Model):
    _name = 'pirma.klasė'

    def class_method(self):
        print "nepakeista"

>>> PirmaKlasė.class_method()
nepakeista

class AntraKlasė(Model):
    _inherit = 'pirma.klasė'

    def class_method(self):
        print "pakeista"

>>> PirmaKlasė.class_method()
pakeista
```

Tokia sistema leidžia Odoo framework'ui turėti vieną kertinių jo privalumų - modulizavimą su praplėtimu. Tai yra, Odoo galimybes galima praplėsti visiškai atskirai parašytu moduliu, kuris, nekeisdamas nė eilutės kitų modulių kodo, pakeičia jų veiksmus. Būtent tai mes šių dirbtuvių metu ir padarysime.

*Na, bet grįžkime prie freimworko savybių.*

**slide: Odoo ORM**

Minėjau, kad Odoo grįsta objektiniu programavimu bei turi ORM, Objektų Ryšių Valdymo, metodus. Šie metodai suteikia supaprastina bendravimą su duomenų baze. 

**slide: ORM: lengviau kurti lenteles duomenų bazėje**

* Lengviau kurti lenteles duomenų bazėje.

Kiekviena klasė, kildinta iš `Model` arba `TransientModel`, yra lentelė duomenų bazėje, o tokios klasės egzempliorius - viena lentelės eilutė. Tam tikru būdu nurodyti klasės kintamieji atitinka lentelės stulpelius. 

Kode klasė - lentelė, kintamasis `vardas` - lentelės stulpelis, laukas. Klasės instancija - viena duombazės eilutė.

```
class DėstytojasKlasė(Model):
    _name = 'dėstytojas'

    vardas = fields.Char()

    def pravesti_paskaitą(self):
        print 'Vedama paskaita'
```

**slide: Lengviau nurodyti ir panaudoti objektų ryšius**

* Su ORM - Lengviau nurodyti ir panaudoti objektų ryšius. 

Pavyzdžiui, yra `Dėstytojas` ir mokomasis `Dalykas`. Vienas dėstytojas gali dėstyti skirtingus dalykus, tačiau vieną dalyką dėsto tik vienas dėstytojas. Tai aprašoma `Many2one` ryšiu per klasės kintamąjį.

```
class DėstytojasKlasė(Model):
    _name = 'dėstytojas'

    def pravesti_paskaitą(self):
        print "Vedama paskaita"

class DalykasKlasė(Model):
    _name = 'dalykas'

    dėstytojas = fields.Many2one('dėstytojas')
```

**slide: Lengviau nurodyti ir panaudoti objektų ryšius pt2**

Ir dabar dalyko `Dėstytojo` klasę ir jos metodus galima pasiekti labai paprastai. Kviečiame `DalykoKlasę` ir jos lauką, siejantį Dalyką su Dėstytoju. Tai nukreipia į `DėstytojoKlasę` ir dabar galime naudotis jos metodais ir laukais.

```
>>> DalykasKlasė.dėst.pravesti_paskaitą()
Vedama paskaita
>>> print DalykasKlasė.dėst.vardas
'vardas' reikšmė iš duomenų bazės
```

**slide: Paprastesnė darbo su DB įrašais sintaksė**

* Su ORM - Paprastesnė darbo su duomenų bazės įrašais sintaksė.

```
car = self.env['car_park'].browse(1)
car.write({'color': 'black'})
car.write({'headlamp': 'neon green'})
```

Vietoje sudėtingos SQL užklausos, tereikia nurodyti įrašą, ką nori su juo padaryti bei kokia informacija turi būti perduota, ir freimworkas savo darbą atliks automatiškai - sugeneruos atitinkamą SQL užklausą.

**slide: Papr DB sintaksė pt2**

```
UPDATE car_park SET color = 'black' WHERE id = 1;
UPDATE car_park SET headlamp = 'neon green' WHERE id = 1;
```

Kaip matote, nereikia mokėti SQL kalbos, kad atliktumėte daugumą paprastų veiksmų su duomenų baze.

Čia baigiasi įvadas į odoo ir python'ą.

**slide: Klausimai?**

Klausimai?