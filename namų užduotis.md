# VKKD2016 odoo dirbtuvių namų užduotis

Kliento pageidavimu, jis norėtų gauti priminimą, kad artėja sertifikato galiojimo pabaigos data.

## Užduotis

Sukurti `hr.employee.certificate` praplečiantį modulį, kuris išsiųstų laišką su priminimu, kai iki sertifikato galiojimo pabaigos lieka 30 dienų.

## Apybraiža

Ši užduotis bus vykdoma pasinaudojant `cron` - užduočių planuotoju. Reikia pridėti užduotį patikrinti visų sertifikatų galiojimo datas ir išsiųsti priminimą. Užduotį vykdyti kiekvieną dieną. Priminimas siunčiamas tik vieną kartą.

## Užduoties vykdymas

### Etapų formulavimas
Analizuojant užduotį reikia susidaryti etapus, kurie turės būti įvykdyti:

* Pasinaudojus modulio šablonu, pasidaryti naują modulį.

* Sukurti metodą, tikrinantį sertifikatų galiojimo laiką, `_priminti_sert_pabaigą`. 

    - Kadangi reikia tikrinti visus darbuotojo sertifikatus, tokį metodą rašyti logiškiau pačiame `hr.employee`, nei `hr.employee.certificate`. Tad, sukuriame python klasę, kuri praplečia `hr.employee`:

    ```
    class HrEmployee(models.Model):
        _inherit = 'hr.employee'
    ```

    - Sukuriame metodą `_priminti_sert_pabaigą`:

    ```
    def _priminti_sert_pabaigą(self):
        pass
    ```

    - Šis metodas turi atlikti šiuos veiksmus:

        1. Žinoti, kokia yra šiandienos data. Formatas `%Y-%m-%d`. Siūloma naudotis python `date.strftime` arba vidiniu odoo metodu `fields.Date.from_string()`.

        2. Žinoti, kokia data yra už 30 dienų.

        3. Surinkti visus dominančius sertifikatus, priklausančius darbuotojui. Siūloma naudotis tiesioginiu ryšiu tarp `hr.employee` ir `hr.employee.certificate` per `certificate_ids` laukelį arba susirasti juos tiesiai iš lentelės su `self.env['hr.employee.certificate'].search([domain])`.

            Galiojimo pabaigos data saugoma `valid_to` laukelyje.

            [`domain` dokumentacija.][1]

        4. Išsiųsti laišką kiekvienam besibaigiančio galiojimo sertifikato savininkui. Tai siūloma deleguoti kitam metodui, pvz., `_siųsti_sert_priminimą`.

* Sukurti metodą `_siųsti_sert_priminimą`. Jame turi būti:

    1. Sugeneruojamas laiško šablonas iš xml. 

        Naudojamas `email.template` metodas `generate_email_batch(template_id, [employee_id])`.
        Čia `template_id` yra konkretaus šablono, sukurto xml faile jūsų modulyje įrašas duomenų bazėje. Bet kokie įrašai, kurie kuriami pačio modulio (ne per serverio grafinę aplinką), gali būti pasiekti per išorinį įrašo id. Python metodais: `self.env['lenteles.pavadinimas'].ref('modulio_pavadinimas.išorinis_įrašo_id')`.

        [`ref` dokumentacija.][2]

    2. Į kintamąjį sudedamos reikšmės:

        - `'subtype_id': self.env.ref('mail.mt_comment').id`,
        - `'record_name': self._name`,
        - `'type': 'email'`,
        - `'body': sugeneruotas_šablonas.get('body', False)`,
        - `'model': sugeneruotas_šablonas.get('model', False)`,
        - `'res_id': sugeneruotas_šablonas.get('res_id', False)`,
        - `'subject': sugeneruotas_šablonas.get('subject', False)`,
        - `'email_from': 'jūsų@el.paštas'`.

    3. Sukuriamas el. laiškas su šiomis reikšmėmis. Naudojamas `mail.message` modulio `create(kintamasis_su_reikšmėmis)` metodas. Šis metodas sukuria laišką, kuris paskui siunčiamas.

* Sukurti el. laiško šabloną.

    - Šablonas kuriamas per xml.

    - Šablono įrašas kuriamas `email.template` lentelėje. Įrašo `id` bus naudojamas `_siųsti_sert_priminimą` metode.

    - Užpildomi lentelės laukai:

        1. `name` - šablono pavadinimas,
        2. `model_id` - modelis, kuriam priklauso šis šablonas, `ref="hr.model_hr_employee"`.
        3. `subject_id` - tekstas, dedamas į antraštę. Galite įterpti sertifikato pavadinimą per `${object.name}` laukelį - jį paskui užpildys odoo.
        4. `body_html` - laiško turinys. 

* Sukurti įrašą `ir.cron` duomenų bazės lentelėje su nurodymu kasdien pasinaudoti `_priminti_sert_pabaigą` metodu.
    
    - `ir.cron` įrašai kuriami per xml.

    [`ir.cron` pavyzdys.][3]

## Baigiamasis žodis

Python 2 nepalaiko ne ASCII rašmenų, todėl lietuviškus kodo fragmentus pakeiskite lotyniškais simboliais.

Kad odoo sistema atpažintų esant naują modulį, reikia perkrauti serverį (arba vagrant'ą), kaip administratoriui pridėti prie savo vartotojo "Technical features" varnelę (*Settings -> Users -> Administrator -> Edit*), ir atnaujinti modulių sąrašą per *Settings -> Modules -> Update Modules List -> Update*. Naujasis modulis atsiras *Local Modules* sąraše.

Čia pateikti nurodymai yra tik patariamojo tipo. Užduotį atlikite taip, kaip jums atrodo geriausiai, juk sprendimo kelių yra tikrai ne vienas.

Baigtus modulius siųskite el. paštu practice@boolit.eu.

[1]: https://www.odoo.com/documentation/8.0/reference/orm.html#reference-orm-domains
[2]: http://pythonhosted.org/OdooRPC/tuto_browse.html
[3]: https://github.com/odoo/odoo/blob/8.0/addons/membership/membership_data.xml